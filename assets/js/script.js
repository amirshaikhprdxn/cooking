// hamburger code starts
$('.hamburger').click(function(e) {
	e.preventDefault();
	$this = $(this);
	$this.siblings('ul').toggleClass('block');
	$this.children('.line1').toggleClass('ham1');
	$this.children('.line2').toggleClass('ham3');
	$this.children('.line3').toggleClass('ham2');
});

// image filter code starts
$(".tab").click(function(event){
	event.preventDefault();
	$tab_content = $(".tab-content");
	$(".tab").removeClass('active');
	$(this).addClass('active');
	var data_attr = $(this).attr('data-tabs');
	// if data attr value of tab contains all display all images
	if (data_attr == "all") {
		$tab_content.removeClass("hide-content");
	} else {
		$tab_content.addClass("hide-content");
		// diplay images if data attr value of tab and tab content is equal
		$tab_content.each(function(index,item) {
			if (data_attr == $(this).attr('data-tabs')){
				$(this).removeClass("hide-content");
			}
		});
	}
});

// form validation starts
var regEx, valid;

$('.form-detail').click(function(e) {
	e.preventDefault();
	var email = $('#email').val();

	$('input').siblings('span').remove();

	if (!email.length) {
		validateEmptyField($('#email'));
	}	else {
		regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		checkValidation(regEx, $('#email'), email, "Enter a valid input");
	}
});
// out of focus and input type=email is empty show error msg
$('input[type="email"]').blur(function(){
	if (!$(this).val().length) {
		if ($(this).siblings('span').hasClass('invalid-input')) {
			$(this).siblings('span').remove();
		}
		if (!$(this).siblings('span').hasClass('error')) {
			$(this).after('<span class="error">This field is required</span>');
		}
	} else {
		regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		checkValidation(regEx, $(this), $(this).val(), "Enter a valid input");
	}
});
// on focus hide error msg
$('input').focus(function(){
	if ($(this).siblings('span').hasClass('error')) {	
		$(this).siblings('span').remove();
	}
	if ($(this).siblings('span').hasClass('invalid-input')) {	
		$(this).siblings('span').remove();
	}
});
// if expression is invalid display err msg
function checkValidation(regEx, element, value,errorMsg) {
	var valid = regEx.test(value);
	if(!valid) {
		if (!element.siblings('span').hasClass('invalid-input')) {
			element.after('<span class="invalid-input">'+errorMsg+'</span>');
		}
	}
}
// displays required field err
function validateEmptyField(element) {
	element.focus();
	if (!element.siblings('span').hasClass('error')) {
		element.after('<span class="error">This field is required</span>');
	}
}

// read more code starts
$('.read-more').click(function(event){
	event.preventDefault();
	$this = $(this);
	$li = $('.load-more');
	// if clicked anchor has active class display content else hide content
	if (!$this.hasClass('active')) {
		$li.fadeIn()
		$this.text('see less editors collections');
		$this.addClass('active');
	} else {
		$li.fadeOut()
		$this.text('see all editors collections');
		$this.removeClass('active');
	}
});